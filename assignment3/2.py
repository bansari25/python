#a03p02a
l1=[x for x in range(1,11)]
print("l1=",l1)

#a03p02b
l2=[y for y in range(10,110,10)]
print("l2=",l2)

#a03p02c
l3=['python','django','flask','string','function','classes']
print("l3=",l3)

#a03p02d
l4={'l1':'list l1','l2':'list l2','l3':'list l3'}
print("l4=",l4)
for k,v in l4.items():
    print("%s-->%s" %(k,v))

#a03p02e
main_list_1=[]
main_list_1.append(l1)
main_list_1.append(l2)
main_list_1.append(l3)
main_list_1.append(l4)
print("main_list_1=",main_list_1)

main_list_2=[]
main_list_2.extend(l1)
main_list_2.extend(l2)
main_list_2.extend(l3)
main_list_2.extend(l4)
print("main_list_2=",main_list_2)

#a03p02f
l5=(l1*2)
print("l5=",l5)

#a03p02g
main_list_1.append(l5)
print("main_list_1=",main_list_1)

#a03p02h
a=main_list_1.count(1)
print("occurences of integer 1 in main_list=",a)

b=main_list_2.count(1)
print("occurences of integer 1 in main_list=",b)